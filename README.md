# Silithid Bug Counter

Counts how many silithid bugs die around you

## How does it work

`/bugcounter` `/bc` to bring up the count. It will display automatically once it detects a bug's death.
For optimal experience is recommended to extend combat log range: `/script SetCVar("CombatLogRangeCreature","200")`

## Requiremens

EmeraldFramework https://gitlab.com/woblight/EmeraldFramework
