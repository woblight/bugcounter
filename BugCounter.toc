## Interface: 11200
## Title: Silithid Bug Counter
## Notes: Counts bugs slain in Silithus (Counting bugs is not guaranteed to help sleep)
## Version: 1
## Author: WobLight
## Deps: EmeraldFramework
## SavedVariables: BugCounterSettings
BugCounter.lua
