local function prepare(template) --courtesy of shagu
    template = gsub(template, "%(", "%%(") -- fix ( in string
    template = gsub(template, "%)", "%%)") -- fix ) in string
    template = gsub(template, "%d%$","")
    template = gsub(template, "%%s", "(.+)")
    return gsub(template, "%%d", "(%%d+)")
end

local EventHandler = CreateFrame("FRAME")

BugCounter = EFrame.Object()

BugCounter:attach("total")
BugCounter:attach("session")
BugCounter:attach("items")

function BugCounter:onTotalChanged(t)
    BugCounterSettings.total = t
end

local BugCounterWindow

local deathRex = prepare(UNITDIESOTHER)

local bugs = {}
bugs["Supreme Silithid Flayer"] = true
bugs["Hive'Ashi Stinger"] = true
bugs["Hive'Ashi Worker"] = true
bugs["Hive'Ashi Defender"] = true
bugs["Hive'Ashi Sandstalker"] = true
bugs["Hive'Ashi Swarmer"] = true
bugs["Hive'Zora Waywatcher"] = true
bugs["Hive'Zora Tunneler"] = true
bugs["Hive'Zora Wasp"] = true
bugs["Hive'Zora Reaver"] = true
bugs["Hive'Zora Hive Sister"] = true
bugs["Hive'Regal Ambusher"] = true
bugs["Hive'Regal Burrower"] = true
bugs["Hive'Regal Spitfire"] = true
bugs["Hive'Regal Slavemaker"] = true
bugs["Hive'Regal Hive Lord"] = true
bugs["Hive'Ashi Drone"] = true
bugs["Hive'Ashi Ambusher"] = true


function EventHandler:CHAT_MSG_COMBAT_HOSTILE_DEATH()
    local _,_,name = strfind(arg1, deathRex)
    if bugs[name] then
        BugCounter.session = BugCounter.session + 1
        BugCounter.total = BugCounter.total + 1
        BugCounter_ShowCount()
    end
end

function EventHandler:BAG_UPDATE()
    local count = 0
    for b = 0, 4 do
        for s = 1, GetContainerNumSlots(b) do
            local link = GetContainerItemLink(b,s)
            if link and strfind(link, "[Silithid Carapace Fragment]",0,1) then
                local _, c = GetContainerItemInfo(b,s)
                count = count + c
            end
        end
    end
    BugCounter.items = count
end

function EventHandler:ADDON_LOADED()
    if strlower(arg1) == "bugcounter" then
        if not BugCounterSettings then
            BugCounterSettings = {total = 0}
        end
        setmetatable(BugCounterSettings, { __index = BugCounterSettingsDefaults })
        EventHandler:UnregisterEvent("ADDON_LOADED")
        EventHandler:RegisterEvent("CHAT_MSG_COMBAT_HOSTILE_DEATH")
        BugCounter.total = BugCounterSettings.total
        BugCounter.session = 0
    end
end

function EventHandler:process()
    local handle = EventHandler[event]
    if handle then
        handle(EventHandler)
        return
    end
end



local function UIInit()
    BugCounterWindow = EFrame.Window()
    BugCounterWindow.visible = false
    BugCounterWindow.title = "BugCounter"
    BugCounterWindow.centralItem = EFrame.ColumnLayout(BugCounterWindow)
    BugCounterWindow.centralItem.spacing = 2
    local killLabel = EFrame.Label(BugCounterWindow.centralItem)
    killLabel.text = "Bugs Killed"
    killLabel.n_text:SetFont(killLabel.n_text:GetFont(), 24)
    local sessionText = EFrame.Label(BugCounterWindow.centralItem)
    sessionText.text = EFrame.bind(function() return format("This session: %d", BugCounter.session) end)
    sessionText.n_text:SetFont(killLabel.n_text:GetFont(), 20)
    local totalText = EFrame.Label(BugCounterWindow.centralItem)
    totalText.text = EFrame.bind(function() return format("Total: %d", BugCounter.total) end)
    totalText.n_text:SetFont(killLabel.n_text:GetFont(), 20)
    local itemsText = EFrame.Label(BugCounterWindow.centralItem)
    itemsText.text = EFrame.bind(function() return format("Fragments: %d", BugCounter.items) end)
    itemsText.n_text:SetFont(killLabel.n_text:GetFont(), 20)
    
    BugCounterWindow.background.color = {0.2,0.2,1,0.33}
    
    BugCounterWindow.dragActiveChanged:connect(function (dragged)
        if not dragged then
            BugCounterSettings.windowX = BugCounterWindow.x
            BugCounterSettings.windowY = BugCounterWindow.y
        end
    end)
    function BugCounterWindow:onVisibleChanged(visible)
        if visible then
            if BugCounterSettings.windowX and BugCounterSettings.windowY then
                self.x = BugCounterSettings.windowX
                self.y = BugCounterSettings.windowY
            else
                self.x = self.parent.width/2 - self.width/2
                self.y = self.parent.height/2 - self.height/2
            end
        end
    end
end

function BugCounter_ShowCount()
    if not BugCounterWindow then
        UIInit()
    end
    BugCounterWindow.visible = true
end

local function chatcmd()
    if not BugCounterWindow then
        UIInit()
    end
    BugCounterWindow.visible = not BugCounterWindow.visible
end

SLASH_BUGCOUNTER1 = "/bugcounter"
SLASH_BUGCOUNTER2 = "/bc"
SlashCmdList["BUGCOUNTER"] = chatcmd

EventHandler:SetScript("OnEvent", function() EventHandler:process() end)
EventHandler:RegisterEvent("ADDON_LOADED")
EventHandler:RegisterEvent("BAG_UPDATE")
